import QtQuick 6.2
import QtQuick.Controls 6.2
import QtQuick.Layouts  6.2

import de.igh.qtpdcom 1.4
import de.igh.pd 1.0

Item {
    RowLayout {
        anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 10

        ListModel {
            id: eventModel

            ListElement {
                key: "Reset"
                value: 0
            }
            ListElement {
                key: "Emergency"
                value: 1
            }
            ListElement {
                key: "Alert"
                value: 2
            }
            ListElement {
                key: "Critical"
                value: 3
            }
            ListElement {
                key: "Error"
                value: 4
            }
            ListElement {
                key: "Warning"
                value: 5
            }
            ListElement {
                key: "Notice"
                value: 6
            }
            ListElement {
                key: "Info"
                value: 7
            }
            ListElement {
                key: "Debug"
                value: 8
            }
        } // ListModel

        GroupBox {
            title: "Message Switches"
            Layout.preferredWidth: 250
            Layout.fillHeight: true
            Layout.fillWidth: false
            ColumnLayout {
                anchors.fill: parent

                Repeater {
                    model: 5

                    ColumnLayout {
                        Label {
                            text: "Event " + (modelData + 1)
                        }
                        PdComboBox {
                            Layout.fillWidth: true
                            model: eventModel
                            path: "/Event/State#" + modelData
                            indexRole: "value"
                            textRole:"key"
                        }
                    } // ColumnLayout
                } // Repeater

                Item {
                    Layout.fillHeight: true
                }
            } // ColumnLayout
        } // GroupBox

        GroupBox {
            title: "Message Display"
            Layout.fillHeight: true
            Layout.fillWidth: true
            ColumnLayout {
                anchors.fill: parent

                MessageTableView {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    model: MessageModel {
                        process: DefaultProcess
                    }
                }
            } // ColumnLayout
        } // GroupBox
    }
}
