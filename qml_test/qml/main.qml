/****************************************************************************
**
** Example application for qml pdwidgets integration
**
** Copyright (C) 2019 Wilhelm Hagemeister
** Contact: hm@igh.de
**
**
****************************************************************************/

import QtQuick 6.2
import QtQuick.Layouts 6.2
import QtQuick.Controls 6.2
import QtQuick.Controls.Material 6.2
import QtQuick.Window 6.2
import QtQuick.Dialogs 6.3

import de.igh.qtpdcom 1.4
import de.igh.pd 1.0
import de.igh.pd.style 1.0

ApplicationWindow {
    id: appContainer
    width: 1024
    height: 600
    visible: true
    title: "PD-QML"

    Material.theme:themeSwitch.checked?Material.Dark:Material.Light
    Material.primary:Material.background
    Material.accent:Solarized.green
    FontLoader {
        source: "qrc:/de.igh.pd1/imports/de/igh/pd/FontAwesome.otf"
    }

    LoginManager {
        id: loginManager

        onNeedCredentials: {
            loginDialog.open()
        }
        onLoginFailed: {
            loginFailedDialog.open()
        }
    }

    Binding {
        target: DefaultProcess
        property: "sslCaMode"
        value: connectionDialog.useTLS ? Process.SslCaMode.DefaultCAs : Process.SslCaMode.NoTLS
    }

    Component.onCompleted: {
        DefaultProcess.loginManager = loginManager
        DefaultProcess.connectToHost("localhost", 2345)
        // force load of PdTranslator singleton
        PdTranslator.retranslate()
    }

    MessageDialog {
        id:noFunctionDialog
        text: "No function."
    }


    MessageDialog {
        id:loginFailedDialog
        text: qsTr("Login failed")
    }

    ConnectionDialog {
        id:connectionDialog
        onApplied: {
            DefaultProcess.disconnectFromHost()
            DefaultProcess.connectToHost(host,port)
        }
        onAccepted: {
            DefaultProcess.disconnectFromHost()
            DefaultProcess.connectToHost(host,port)
        }
    }

    LoginDialog {
        id: loginDialog
        loginManager: loginManager
    }

    Menu {
        id:menu
        implicitWidth: 260
        Action {
            text: "Open"
            icon.source:"qrc:/images/folder-open.svg"
            onTriggered:{
                connectionDialog.open()
            }
        }
        Action {
            id: loginAction
            text: qsTr("Login...")
            icon.source:"qrc:/images/sign-in.svg"
            onTriggered: {
                loginDialog.open()
            }
        }
        Action {
            text: "Logout"
            icon.source:"qrc:/images/sign-out.svg"
            onTriggered: {
                loginManager.logout()
            }
        }
        Action {
            text: "Close"
            icon.source:"qrc:/images/bolt.svg"
            onTriggered: {
                Qt.callLater(Qt.quit);
            }

        }
    }  //Menu ----------------------------------------

    header: ToolBar {
        RowLayout {
            spacing: 5
            anchors.fill: parent
            ToolButton {
                Layout.leftMargin:10
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf0c9"
               onClicked: menu.open()
            }

            Label{
                Layout.fillWidth:true
                text:""
            }

            ToolButton { //Reload
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf01e"
                ToolTip.visible: hovered
                ToolTip.text: qsTr("reload parameter set")
                enabled: true
                onClicked: {}
            }

            PdDigital { //Supply Voltage
                id:supplyVoltage
		        connection {
                    path:"/Taskinfo/0/ExecTime"
                    period:0.5
                    scale:1000000
		        }
                font.pixelSize:20
                decimals:2
                suffix:" µs"
            }

            PdVariable {
                id:controllerActivate
                connection.path:"/osc/enable"
            }

            PdToolButton {
                id:motorToolButton
                path:"/osc/enable"
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf085"

            }

            PdToolButton {
                path:"/osc/enable"
                font.family: "FontAwesome"
                activeColor:"red"
                font.pixelSize: 20
                text: "\uf0e7"
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Overcurrent")
            }

            PdLed {
                connection {
                    path: "/osc/enable"
                    period: 0.0
                }
            }

            PdBar {
                Layout.rightMargin:10
                from:0
                to:10
                suffix:" µs"
                decimals:2
		        connection {
                    scale:1e6
                    period:0.1
                    path:"/Taskinfo/0/ExecTime"
		        }
            }
	    Label {
                text:qsTr("Dunkel")
                leftPadding: 10
            }
            Switch {
		id:themeSwitch
		checked:(appContainer.Material.theme == Material.Dark)
            }

        } //RowLayout
    } //header

    footer:TabBar {
        id: bar
        width: parent.width
        currentIndex: view.currentIndex

        TabButton {
            text: qsTr("Page 1")
            icon.source:"qrc:/images/cogs.svg"
        }
        TabButton {
            text: qsTr("Page 2")
            icon.source:"qrc:/images/plug.svg"
        }

        TabButton {
            text: qsTr("Messages")
            icon.source: "qrc:/images/book-solid.svg"
        }

        TabButton {
            text: qsTr("SVG")
            icon.source:"qrc:/images/wrench.svg"
        }
    } //footer

    SwipeView {
        id: view
        interactive:true
        currentIndex: bar.currentIndex
        anchors.fill: parent

        onCurrentIndexChanged: {
        }

        ChannelPane {
        }

        ParameterPane {
        }
        MessagesPane {
        }
        SvgPane {
        }
    } //SwipeView
} //Application Window
