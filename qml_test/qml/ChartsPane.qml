/****************************************************************************
**
** Copyright (C) 2019 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** project: pdWidgets
**
****************************************************************************/

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts

import QtCharts
import QmlTest

import de.igh.pd 1.0
import de.igh.pd.style 1.0

ChartView {  //Scrolling graph
    id:chart
    antialiasing: false
    animationOptions: ChartView.NoAnimation
    margins.top:0
    margins.bottom:0
    margins.left:0
    margins.right:0
    //change theme according to main theme
    theme: Material.theme ==
    Material.Dark?ChartView.ChartThemeDark:ChartView.ChartThemeQt
    //FIXME why are no Material Themes available as chart theme???
    backgroundColor:Material.background
    /*
    themeChanged: {
	backgroundColor="transparent" //Material.background
    }
*/
    legend.font:Qt.font({pixelSize:16})
    property double timeRange:10

    TouchEditDialog {
        id:chartTimeRange
        suffix:"s"
        lowerLimit:1
        upperLimit:100
        decimals:0
        title:"Time Range"
        onAccepted:{
            chart.timeRange = value
        }
    }
    MouseArea {
        anchors.fill:parent
        onClicked:{
            chartTimeRange.value = chart.timeRange
            chartTimeRange.open()
        }
        z:2
    }
    
    ValueAxis {
        id:axisX
        labelsFont:Qt.font({pixelSize:16})
        tickCount:5
        titleText:"Time / s"
        titleFont:Qt.font({pixelSize:12})
        max:0
        min:-chart.timeRange
    }
    ValueAxis {
        id:axisY1
        min:-10
        max:10
        tickCount:5
        labelsFont:Qt.font({pixelSize:16})
        titleText:"Trig / V"
        titleFont:Qt.font({pixelSize:12})
    }
    ValueAxis {
        id:axisY2
        min:0
        max:500
        tickCount:5
        // minorTickCount:2
        labelsFont:Qt.font({pixelSize:16})
        titleText:"Saw / °C"
        titleFont:Qt.font({pixelSize:12})
    }

    LineSeries {
        name: "cos"
        axisX:axisX
        axisY:axisY1
        color: Solarized.blue
        useOpenGL:true
        width:2
        property var data: PdSeriesData {
            connection: {
                "path":"/osc/cos",
                "period":0.01,
                "scale":1
            }
            timeRange:chart.timeRange
        }
    }
    LineSeries { //Current
        name: "sin"
        axisX:axisX
        axisY:axisY1
        color: Solarized.red
        useOpenGL:true
        width:2
        property var data: PdSeriesData {
            connection: {
                "path":"/osc/sin",
                "period":0.01
	        }
            timeRange:chart.timeRange
        }
    }

        LineSeries { //dreieck
        name: "triangle"
        axisX:axisX
        axisY:axisY2
        color: Solarized.green
        useOpenGL:true
        width:2
        property var data: PdSeriesData {
            connection: {
                "path":"/SawTooth",
                "sampleTime":0.1
            }
            timeRange:chart.timeRange
        }
    }

    Timer { //refresh the Chart
        interval: 1 / 20 * 1000 // 20 Hz
        running: true
        repeat: true
        onTriggered: {
            for(var i = 0;i<chart.count;i++) {
                chart.series(i).data.update(chart.series(i))
            }
        }
    }
} //Chartview  ----------------------
