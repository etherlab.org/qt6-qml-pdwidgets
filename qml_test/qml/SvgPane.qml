/****************************************************************************
**
** Copyright (C) 2019 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** project: pdWidgets
**
****************************************************************************/

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts

import de.igh.pd 1.0
import de.igh.qtpdcom 1.4

Item {
    LiveSvg {
        id:svg
        anchors.fill: parent
        source: "qrc:/images/svg_example.svg"
		invert: appContainer.Material.theme == Material.Dark
		Button {
			id:myButton
			LiveSvg.overlayNodeId:"myButton"
			text:"press me"
		}
		PdLed {
			LiveSvg.overlayNodeId:"squareRect1"
			connection {
				path:"/osc/enable"
			}
			color:"red"
			blinking:true
			invert:true
		}
		PdDigital {
			LiveSvg.overlayNodeId:"processValue1"

			connection {
				path:"/Taskinfo/0/Period"
				period:0.1
				scale:1000
			}
			suffix:"ms"
			font.bold: true
			font.pixelSize:20
		}
		Label {
			LiveSvg.overlayNodeId:"nothingHappens"
			text:"nothing happens"
			visible:myButton.pressed

			Rectangle {
				anchors.fill: parent
				color: "transparent"
				border {
					color: "red"
					width: 1
				}
			}
		}

		PdVariable {
			id: sin
			connection {
				path: "/osc/sin"
				period: 0.05
			}
		}
		DynamicSvgAttribute {
			nodeId: "dynamic_text"
			attribute {
				name: "transform"
				value: sin.connected ? ("translate("+sin.value+")") : ""
			}
		}
    }
} //Item
