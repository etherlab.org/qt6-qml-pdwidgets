/****************************************************************************
 *
 * Copyright (C) Ingenieurgemeinschaft IgH
 *               2022 Jonathan Grahl <jonathan.grahl@igh.de>
 *               2022 Wilhelm Hagemeister <wilhelm.hagemeister@igh.de>
 *
 ***************************************************************************/
//*************************************************************************
//*************************************************************************

import QtQuick 6.2
import QtQuick.Controls 6.2
import QtQuick.Layouts 6.2

TableView {
    id: view

    columnSpacing: 1
    rowSpacing: 1
    clip: true
    flickableDirection: Flickable.VerticalFlick

    // There are two table columns: message and time
    delegate: Rectangle {
        implicitWidth: (model.column == 0) ? view.width - 500 : 250
        implicitHeight: 48
        color: model.row % 2 ? palette.base : palette.alternateBase

        ToolTip {
            id: toolTip

            text: (typeof model.toolTip == "string") ? model.toolTip : ""
            anchors.centerIn: parent
            implicitWidth: 400
        }

        RowLayout {
            anchors.fill: parent
            spacing: 5

            Image {
                Layout.leftMargin: 10
                source: model.decorationPath.replace(":/", "qrc:/")
                sourceSize.width: 20
                sourceSize.height: 20
            }

            Label {
                id: text

                text: model.display
                Layout.fillWidth: true
            }

            ToolButton {
                id: descriptionButton

                Layout.leftMargin: 10
                text: "\uf0ad"
                font.family: "FontAwesome"
                onClicked: {
                    toolTip.visible = true;
                    toolTip.parent = descriptionButton;
                }
                visible: typeof model.toolTip === "string" && model.toolTip.length > 0
            }

        }

    }

}
