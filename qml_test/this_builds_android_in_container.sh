#!/bin/sh

set -ex

# allow us to run `git` even though we don't own the directory
git config --global --add safe.directory /project

mkdir build && cd build

cmake -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK_ROOT}/build/cmake/android.toolchain.cmake \
    -DANDROID_ABI=${PD_ANDROID_ABI} \
    -DANDROID_PLATFORM=android-${PD_ANDROID_API} \
    -DCMAKE_FIND_ROOT_PATH=${PD_PREFIX} \
    -DANDROID_SDK_ROOT=$ANDROID_SDK_ROOT \
    -DCMAKE_ANDROID_STL_TYPE=c++_shared \
    /project

make -j4

# copy back to user
mkdir -p /tmp/apk
find . -name '*.apk' -exec cp {} /tmp/apk/ \;
echo done...
