------------------------------------------------------------------------------
vim: tw=78 syntax=markdown
------------------------------------------------------------------------------

# General Information

This is the Pd QML Widgets library, a process-data aware widget library for
Qt/QML from the EtherLab project. See
https://gitlab.com/etherlab.org/pd-qml-widgets

------------------------------------------------------------------------------

# License

The Pd QML Widgets library is licensed unter the terms and conditions of the
GNU Lesser General Public License (LGPL), version 3 or (at your option) any
later version.

------------------------------------------------------------------------------

# Prerequisites

   - Qt6
   - PdCom 5 (see http://etherlab.org/en/pdcom)
   - QtPdCom 1

------------------------------------------------------------------------------

# Building and Installing

## cmake
The build process is handled by cmake. To build and install, call:


```
mkdir build && cd build
cmake ..
make install
```

To change the installation path,
call cmake with `-DCMAKE_INSTALL_PREFIX=/my/custom/prefix`.
If PdCom5 and/or QtPdCom1 is installed at a non-standard location,
use `-DCMAKE_PREFIX_PATH=/pdcom/install/prefix`.
Multiple paths can be joined by semicolon (`;`).
You can even use this option to point CMake to a build directory of
PdCom5 and/or QtPdCom1, this adds the possibility to use a version
of PdCom5 and/or QtPdCom1 which is not installed at all.

To import this library into your own application,
add the following to your `CMakeLists.txt`:
```cmake
find_package(Qt6QmlPdWidgets1 REQUIRED)
...
qt_add_qml_module(
   IMPORT_PATH "${PD_QML_ROOT_PATH};/my/other/imports"
)
```
All dependencies are imported and linked automatically.
The `CMAKE_PREFIX_PATH` argument works here, too.
`PD_QML_ROOT_PATH` is populated by `find_package` and
contains the path of the QML Widget import path.
This is usually `/usr/lib64/qt6/qml`.

If you want to link in this library and all QML widgets statically,
please have a look at the QmlTest example and the corresponding CMake options.

------------------------------------------------------------------------------


Have fun!

------------------------------------------------------------------------------
