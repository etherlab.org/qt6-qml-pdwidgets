Version 2.0 of PdQmlComponents

 Copyright (C) 2021 Wilhelm Hagemeister
 Contact: hm@igh.de
 
 These files are part of the QtPdWidgets library.

 The QtPdWidgets library is free software: you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the License,
 or (at your option) any later version.

 The QtPdWidgets library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with the QtPdWidgets Library. If not, see
 <http://www.gnu.org/licenses/>.

Changes to version 1.0

- Classes which usually are transmitted from the process with
  the "Pd.Event" transmission type e.g. PdSwitch, PdLed,....

  have a convenience property: "path".


  This allows to quickly configure the variable connection.

  !!!!!
  Using the alias: "path" relies on the "default" process connection.
  So Pd::Process::defaultProcess must be valid.
  !!!!!

- Material style has been moved to +material (following the guidelines of Qt)

- "sampleTime" get's "period"

Deprecated v1.0 components are:
- PdButton: use PdPushButton instead
- PdVectorIndicator: renamed to PdVectorLed.qml
- PdToolButton

Renamed:
- PdDigital to PdVectorDigital
- PdDigital is now always a scalar

Removed components are:
- PdVectorLabel 
- PdTumbler: has to be rewritten
