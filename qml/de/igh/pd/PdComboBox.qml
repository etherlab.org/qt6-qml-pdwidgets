/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: on control in pdWidgets
**
****************************************************************************/

import QtQuick 6.2
import QtQuick.Controls 6.2

import de.igh.qtpdcom 1.4

/** ComboBox

The model property of ComboBox has to be set to function property.

Example Model with indexRole:
@code{.qml}
ListModel {
    id:modeModel
    ListElement {key: "None"; value:0}
    ListElement {key: "a few"; value:4}
    ListElement {key: "many"; value:8}
    ListElement {key: "all"; value:10}
}
@endcode

Example Model without indexRole
@code{.qml}
ListModel {
    id:modeModel
    ListElement {key: "None"}
    ListElement {key: "a few"}
    ListElement {key: "many"}
    ListElement {key: "all"}
}
@endcode
*/

ComboBox {
    id: control

    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */

    property alias variable:scalar

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias connection:scalar.connection

    property alias path: scalar.connection.path

    /**
     * Raw Process value
     */
    property alias value: scalar.value

    /**type:string
       IndexRole is written to the realtime process if the model
       has a valid index key. Otherwise the modelIndex is written
       to the process.
    */
    property string indexRole:""

    enabled:scalar.connected
    editable:false  //FIXME how to make this readonly?

    function update(v) {
	var count = 0;
	//determine the length of the model
	//if it is a ListModel it is count
	//if it is a javascript array it is length ?!?
	if(model != undefined) {
	    if (model.count != undefined) {
		count = model.count;
	    } else {
		if(model.length != undefined) {
		    count = model.length;
		}
	    }
	}

	if(count <= 0) {
	    currentIndex = -1;
	    return;
	}

	if(indexRole.length > 0) {
	    for(var i = 0;i<count;i++) {
		if(scalar.value == model.get(i)[control.indexRole]) {
		    currentIndex = i;
		    break;
		}
	    }
	} else {  //indexRole == ''
	    if(v >= 0 && v < count) {
		currentIndex = v;
	    } else {
		currentIndex = -1;
		return
	    }
	}
    }

    onIndexRoleChanged: update(scalar.value)
    onTextRoleChanged: update(scalar.value)
    onModelChanged: update(scalar.value)

    PdVariable {
        id:scalar
	onValueChanged: control.update(value)
    }  //PdVariable

    onActivated: (index) => {
	// index
	if(indexRole.length > 0) {
	    scalar.value = model.get(index)[indexRole]
	} else {
	    scalar.value = index
	}
    }
}
