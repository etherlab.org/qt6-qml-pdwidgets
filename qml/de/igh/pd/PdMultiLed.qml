/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
**
** Same Component as ../PdLed; different style
**
** Todo: Documentation
**
****************************************************************************/
import QtQuick 6.2

import de.igh.qtpdcom 1.4
Item {
    id: indicator
    property int radius:8

    property alias variable:scalar
    property alias connection:scalar.connection

    property alias path:indicator.connection.path

    property var led:_led /** allows to tune properties of the led */

    property bool invert:false

    property bool blinking:false


    property color color:"#00FF00"

    readonly property bool value: ((scalar.value != 0) != invert)

    property bool active:false

    opacity:scalar.connected?1:0.3
    implicitHeight:radius*2
    implicitWidth:radius*2

    Rectangle {
	id:_led
	anchors.fill:parent
	radius:Math.max(width/2,height/2)
	color:indicator.active?indicator.color:Qt.darker(indicator.color,3)
	border.width:0
	antialiasing:true //nice borders
    }
    //im Gegensatz zu active welches beim blinken wegfällt
    PdVariable {
        id:scalar
        onValueChanged: {
            if(value != indicator.invert) {
                if(indicator.blinking) {
                    timer.running = true
                } else {
                    indicator.active = true
                }
            } else {
                timer.running = false
                indicator.active = false
            }
        }
    }
    Timer {
        id:timer
        interval: 500;
        repeat: true;
        onTriggered: {
            indicator.active = (!indicator.active)
        }
    }
}
