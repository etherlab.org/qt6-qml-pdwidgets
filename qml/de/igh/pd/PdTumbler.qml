import QtQuick 6.2
import QtQuick.Controls 6.2


import de.igh.qtpdcom 1.4

//FIXME make a good Tumbler background with indicators
//FIXME Component needs rewrite
Tumbler {
    id: control
    property alias process: scalar.connection.process
    property alias path: scalar.connection.path
    property alias readPath: readScalar.connection.path
    property alias readSampleTime:readScalar.connection.period
    property var stops:[]
    property bool halfSteps:false
    background: Item {
        Item {
            //anchors.verticalCenter:parent.verticalCenter
            anchors.centerIn:parent
            width:parent.width*8/10
            height:control.font.pixelSize*3/2
            //color:"red"
            //border.color:"lightgray"
            //border.width:3

            Canvas { //the arrow
                id: canvas
                anchors.fill: parent

                onPaint: {
                    var h = parent.height
                    var w = parent.width
                    var ctx = getContext("2d")
                    ctx.clearRect(0, 0, width, height)
                    ctx.strokeStyle = "lightgray"
                    ctx.lineWidth = 4
                    ctx.beginPath()
                    ctx.moveTo(2,2)
                    ctx.lineTo(2,h/3)
                    ctx.lineTo(2+w/15,h/2)
                    ctx.lineTo(2,h*2/3)
                    ctx.lineTo(2,h-2)
                    ctx.lineTo(w-2,h-2)
                    ctx.lineTo(w-2,2)
                    ctx.lineTo(0,2)
                    ctx.stroke()
                } //On Paint
            } //Canvas
        }
    } //background
    delegate: Text {
        text: halfSteps? Number.parseFloat(modelData).toFixed(1):modelData
        font.family: control.font.family //FIXME hier fehlen noch mehr Properties
        //              font.pixelSize:control.font.pixelSize

        font.pixelSize: {
            var size = control.font.pixelSize;
            if(control.stops.indexOf(modelData) > -1) {
                size = size*3/2
            }
            return size
        }

        //font.bold: (control.stops.indexOf(modelData) > -1)
        /*
        color: {
            return (control.stops.indexOf(modelData+1) > -1)? "red":"black"
        }
        */
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        //das funktioniert noch nicht bei einem resize des Tumblers, deswegen
        //hier auskommentiert. Es kann mit QT BUG 66799 zu tun haben Hm, 2018-12-07
        opacity: 1.0 //- Math.abs(Tumbler.displacement) / (control.visibleItemCount / 2)
    }
    onHalfStepsChanged:init(stops) //reinit
    function init(stops) {
        //FIXME test if stops is an array
        var max = -Infinity;
        var min = Infinity;
        var model = [];
        //find range
        stops.forEach(function(stop){
            if (stop > max)
            max = stop
            if (stop < min)
            min = stop
        })
        control.stops = stops;
        //FIXME hier noch auf Halfsteps überprüfen
        //contruct model
        for(var i=min;i<max+1;i++) {
            model.push(i);
            if(halfSteps && i<max) {
                model.push(i+0.5);
            }
        }
        control.model = model
        if(model.length>0) {
            currentIndex = 0
            writeValue(model[currentIndex])
        }
    }
/*
    onCurrentIndexChanged: {
        console.log("Tumbler Current index: "+currentIndex)
    }
*/
    onMovingChanged:{
        if(moving) { //bewegung per Touchinteraction
            //console.log("moving");
        }
        else {
            //console.log("notmoving pos: "+currentIndex);
            writeValue(model[currentIndex])
        }
    }

    // -----------------------------------------------------
    // recieve a new value
    function setValue(value) {
        var rvalue = 0
        if(halfSteps) {
            rvalue = Math.round(value*2)/2
        } else {
            rvalue = Math.round(value)
        }

        //FIXME besser?
        for(var i=0;i<model.length;i++) {
            if(model[i] == rvalue) {
                currentIndex = i
                break
            }
        }
    }

    // -----------------------------------------------------
    function writeValue(value) {
        //we first set the currentReadValue to issue an update trigger in the process
        if(readScalar.hasData()) {
            scalar.setValue(readScalar.value)
        }
        scalar.setValue(value)
    }

    // -----------------------------------------------------
    function inc() {
        var index = 0;
        for(index = 0; index < control.stops.length;index++){
            if (control.stops[index] > model[currentIndex]) {
                break
            }
        }

        //FIXME, das geht sicher schöner
        if(index <= control.stops.length-1) {
            for(var i=0;i<model.length;i++) {
                if(model[i] == control.stops[index]) {
                    currentIndex = i
                    break
                }
            }
        }
        writeValue(model[currentIndex])
    }

    // -----------------------------------------------------
    function dec() {
        var index = 0;
        for(index = control.stops.length-1;index>0;index--){
            if (control.stops[index] < model[currentIndex]) {
                break
            }
        }
        if(index >= 0) {
            for(var i=0;i<model.length;i++) {
                if(model[i] == control.stops[index]) {
                    currentIndex = i
                    break
                }
            }
        }

        writeValue(model[currentIndex])
    }

    PdVariable { //write
        id:scalar
    } //PdVariable

    PdVariable { //readBack
        id:readScalar
        connection.process:control.process
        onValueChanged: {
            if(!control.moving) {
                control.setValue(value)
            }
        }
    } //PdVariable
}
