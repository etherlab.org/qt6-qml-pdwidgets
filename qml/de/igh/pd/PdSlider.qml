/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
**
**
****************************************************************************/

import QtQuick 6.2
import QtQuick.Controls 6.2

import de.igh.qtpdcom 1.4

//FIXME: Slider does not react on enabled
//TODO: update value as slider moves

/** Slider
*/
Slider {
    id: control

      /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */

    property alias path: scalar.connection.path

    enabled:scalar.connected

    PdVariable {
        id:scalar
        onValueChanged: {
            if(!control.pressed) { //do not refresh if control is pressed
                control.value = value
            }
        }
    }
    //only write the value at release
    onPressedChanged:{
        if(!pressed) {
            scalar.value = value
        }
    }
}
