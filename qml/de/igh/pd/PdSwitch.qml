/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: not in QtPdWidgets available
**
****************************************************************************/

import QtQuick 6.2
import QtQuick.Controls 6.2

import de.igh.qtpdcom 1.4

/** Switch
 */

Switch {
    id: control

    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */

    property alias path: scalar.connection.path

    /**type:bool
       Invert the function of the switch.
    */
    property bool invert:false

    enabled: scalar.connected

    PdVariable { id:scalar }
    checked: ((scalar.value != 0) != invert)
    onClicked: scalar.value = (control.checked != invert)
}
