/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: name in pdWidgets: Text.h/cpp
**
** !!! consider using PdText if the "hash" has gaps !!!
**
****************************************************************************/


/** Value hash type.
 *
 * This hash shall contain a value object for each possible value to
 * display.
 */

import QtQuick 6.2
import QtQuick.Controls 6.2

import de.igh.qtpdcom 1.4

Label{
    id:control

    /**type:var

       Example hash/model with indexRole:
       @code{.qml}
       hash:[
             {key: "None"}
             {key: "a few"}
             {key: "many"}
             {key: "all"}
            ]
       @endcode

       Example hash/model without indexRole
       @code{.qml}
       hash:["Car","Person","Animal"]
       @endcode
    */
    property var hash:[]
    property alias model:control.hash

    /**type:var
     * this text is shown if no key is found in hash
     */
    property string undefText:"undefined"

    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias path: scalar.connection.path

    /**type:string
     * Text is selected from that role of the model, which key is: IndexRole.
     * When the model has multiple roles, textRole can be set to determine
     * which role should be displayed.
     */
    property string indexRole:""

    /** type:bool
     * This signal is emitted when a mouse is hovered over the control.
     * true: when the mouse moves over the control, false: otherwise
     */
    property alias hovered: ma.containsMouse

    /**type:var
     * Raw Process value
     */
    readonly property alias value:scalar.value

    enabled:scalar.connected

    PdVariable {
        id:scalar
        onValueChanged:{
	    if(control.indexRole.length > 0) {
		if(control.hash[value][control.indexRole] != undefined) {
                    control.text = control.hash[value][control.indexRole];
		} else {
		    control.text = control.undefText
		}
	    } else {
		if(control.hash[value] != undefined) {
                    control.text = control.hash[value]
		} else {
                    control.text = control.undefText
		}
            }
	}
    }
    //add a Mousearea to have the hovered event
    MouseArea {
            id: ma
            anchors.fill: parent
            hoverEnabled: true
    }
}
