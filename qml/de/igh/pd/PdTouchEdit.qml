/****************************************************************************
*
* QML-Widgets for qtPdWidgets
*
* Copyright (C) 2021 Wilhelm Hagemeister
* Contact: hm@igh.de
*
* State: similar component in QtPdWidgets
*
* TODO: Documentation
*
*
****************************************************************************/

import QtQuick 6.2
import QtQuick.Controls 6.2

import de.igh.qtpdcom 1.4

/** Digital display and touch edit.
 */

Label {
    id: control
    property alias variable:scalar
    property alias connection:scalar.connection

    property alias path: scalar.connection.path

    property int decimals:2
    property string suffix:""
    property double lowerLimit:-Number.MAX_VALUE
    property double upperLimit:Number.MAX_VALUE
    property string title: ""

    property alias value: scalar.value

    enabled:scalar.connected


    onDecimalsChanged:{
        scalar.update()
    }
    onSuffixChanged: {
        scalar.update()
    }

    /** type:bool
     * This signal is emitted when a mouse is hovered over the control.
     * true: when the mouse moves over the control, false: otherwise
     */
    property alias hovered: ma.containsMouse

    /**
       this signal is emited if the value is modified by the Dialog.
       It is not emited if the value is modified from other msr connections
       Value is the set value and must not always correspond to the value on the process
       ... but most likely it will. If one read value from the control directly after the
       accepted signal, this will be a process value which is not updated jet.

    */

    signal accepted(double value)

    SystemPalette { id: palette }


    PdVariable {
        id:scalar
        function update() {
            if (value != undefined)
                control.text = value.toLocaleString(Qt.locale(),'f',control.decimals) + control.suffix
        }
        onValueChanged: update()
    }


    Loader { //Loader for dynamic loading of TouchEditDialog
	id: touchEditDialogLoader
	onLoaded: {
	    item.open()
	    item.closed.connect(function() {touchEditDialogLoader.active = false}) //unload Dialog
	}

    }

    function updateValue(value) { //callback für TouchEditDialog
        scalar.value = value
    	control.accepted(value)
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        color:"transparent" //palette.base
        border.color: parent.enabled ? palette.dark:palette.mid
        MouseArea {
	    id: ma
	    hoverEnabled: true
            anchors.fill: parent
            onClicked: {
		touchEditDialogLoader.setSource("TouchEditDialog.qml",
						{"decimals":control.decimals,
						 "suffix":control.suffix,
						 "lowerLimit":control.lowerLimit,
						 "upperLimit":control.upperLimit,
						 "title":control.title,
						 "value":scalar.value,
						 "updateValue":control.updateValue,
						 "parent":control})
		touchEditDialogLoader.active = true

            }
        }
    }

    horizontalAlignment: TextInput.AlignRight
    padding:5
}
