/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: name in pdWidgets: Text.h/cpp
**
** this is a similiar component than PdLabel but it
** uses a map() instead of an array. This allows gaps in the keys.
** Still... "Label" is used as the base qml-component and NOT "Text".
**
****************************************************************************/


/** Value hash type.
 *
 * This hash shall contain a value object for each possible value to
 * display.
 */

import QtQuick 6.2
import QtQuick.Controls 6.2

import de.igh.qtpdcom 1.4

Label{
    id:control

    /** type:var

      Example:
       @code{.qml}
       {
       const map = new Map()
       map.set(0,{"color":"red","text":"Car"});
       map.set(1,{"color":"green","text":"Person"});
       map.set(2,{"color":"green","text":"Animal"});
       map.set(3,{"color":"#FF9800","text":"nothing"});
       return map;
       }
       @endcode
    */
    property var map: ({})


    /**type:var
     * this text is shown if no key is found in hash
     */
    property string undefText: qsTr("undefined")

    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias path: scalar.connection.path

    /**type:string
     * Text is selected from that role of the map, which key is: textRole.
     * When the map has multiple roles, textRole can be set to determine
     * which role should be displayed.
     * With the above example for map
     @code{.qml}
     textRole:"text"
     @endcode
    */
    property string textRole: "text"

    /** type:bool
     * This signal is emitted when a mouse is hovered over the control.
     * true: when the mouse moves over the control, false: otherwise
     */
    property alias hovered: ma.containsMouse

    /**type:var
     * Raw Process value
     */
    readonly property alias value:scalar.value

    /** type:string
     * Prefix is prepended to the value without space!
     */
    property string prefix: ""

    /** type:string
     * Suffix is appended to the value without space!
     */
    property string suffix: ""

    enabled:scalar.connected

    /** type:bool
     * it true, if no key exists in map for the current value
     */

    property bool isUndefined:false

    PdVariable {
        id:scalar
        onValueChanged:{
	    var v = control.undefText;
	    var valid = false;
	    if ((control.map != undefined) &&
		(value in control.map) &&
		(control.textRole in control.map[value])) {
		v = control.map[value][control.textRole];
		valid = true;
	    }
	    control.text = control.prefix + v + control.suffix
	    control.isUndefined = valid;
	}
    }
    //add a Mousearea to have the hovered event
    MouseArea {
            id: ma
            anchors.fill: parent
            hoverEnabled: true
    }
}
