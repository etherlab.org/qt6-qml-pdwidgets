<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="qml/de/igh/pd/LoginDialog.qml" line="+7"/>
        <location/>
        <source>Login...</source>
        <translation>Anmelden...</translation>
    </message>
    <message>
        <location line="+26"/>
        <location/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+15"/>
        <location/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
</context>
<context>
    <name>PdText</name>
    <message>
        <location filename="qml/de/igh/pd/PdText.qml" line="+51"/>
        <source>undefined</source>
        <translation>undefiniert</translation>
    </message>
</context>
<context>
    <name>TouchEditDialog</name>
    <message>
        <location filename="qml/de/igh/pd/TouchEditDialog.qml" line="+293"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>TouchEditDialog_2_2</name>
    <message>
        <location filename="qml/de/igh/pd/TouchEditDialog_2_2.qml" line="+62"/>
        <source>Input method will be changed upon the next invocation of this dialog.</source>
        <translation>Die Eingabemethode wird beim nächsten Öffnen dieses Dialoges umgestellt.</translation>
    </message>
</context>
<context>
    <name>TouchEditDialog_2_3</name>
    <message>
        <location filename="qml/de/igh/pd/TouchEditDialog_2_3.qml" line="+302"/>
        <location line="+220"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-202"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+181"/>
        <source>Value is greater than the limit of </source>
        <translation>Der Wert überschreitet das obere Limit von: </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Value is less than the limit of </source>
        <translation>Der Wert unterschreitet das untere Limit von: </translation>
    </message>
</context>
<context>
    <name>TouchEditNumpad</name>
    <message>
        <location filename="qml/de/igh/pd/TouchEditNumpad.qml" line="+66"/>
        <source>Input method will be changed upon the next invocation of this dialog.</source>
        <translation>Die Eingabemethode wird beim nächsten Öffnen dieses Dialoges umgestellt.</translation>
    </message>
    <message>
        <location line="+165"/>
        <source>Value is greater than the limit of </source>
        <translation>Der Wert überschreitet das obere Limit von: </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Value is less than the limit of </source>
        <translation>Der Wert unterschreitet das untere Limit von: </translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
</TS>
