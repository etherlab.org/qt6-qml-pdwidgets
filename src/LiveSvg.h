/*****************************************************************************
 *
 * Copyright (C) 2018-2025  Bjarne von Horn <vh@igh.de>,
 *                          Wilhelm Hagemeister<hm@igh.de>,
 *
 * This file is part of the Qt6QmlPdWidgets library.
 *
 * The Qt6QmlPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The Qt6QmlPdWidgets library is distributed in the hope that it will be
 *useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Qt6QmlPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_LIVESVG_H
#define PD_LIVESVG_H

#include <QPainter>
#include <QPixmap>
#include <QPointF>
#include <QQmlEngine>
#include <QQmlListProperty>
#include <QScopedPointer>
#include <QSize>
#include <QSizeF>
#include <QString>
#include <QtQuick/QQuickPaintedItem>

namespace Qt6QmlPdWidgets {

class LiveSvgPrivate;
class DynamicSvgAttributePrivate;
class LiveSvg;

class NameValuePair : public QObject
{
    Q_OBJECT
    QML_ANONYMOUS
    Q_PROPERTY(QString name MEMBER name_ NOTIFY nameChanged)
    Q_PROPERTY(QString value MEMBER value_ NOTIFY valueChanged)

  public:
    explicit NameValuePair(QObject *parent = nullptr) : QObject(parent) {}

    QString getName() const { return name_; }
    QString getValue() const { return value_; }

  signals:
    void nameChanged();
    void valueChanged();

  private:
    QString name_, value_;
};

/** Helper class to modify SVG attributes or text nodes.
 *
 * This class can be used in two ways:
 *  - modifying the \c attribute.value of attribute \c attribute.name of XML
 * element \c nodeId . Useful to hide or move an element
 *  - modifying the content of the first child of XML element \c nodeId
 *    Useful to modify a text.
 *
 * If you want to create DynamicSvgAttribute dynamically,
 * for instance with an <a
 * href="https://doc.qt.io/archives/qt-6.0/qml-qtqml-models-instantiator.html">Instantiator</a>,
 * use the \c svg property to assign the corresponding LiveSvg instance.
 */
class DynamicSvgAttribute : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    /// Id of the SVG Element in the \c dynamic layer. Required.
    Q_PROPERTY(
            QString nodeId READ getNodeId WRITE setNodeId NOTIFY nodeIdChanged)
    /// Property group of attribute name and value pair
    Q_PROPERTY(NameValuePair *attribute READ getAttribute)
    /// Value of Text child node
    Q_PROPERTY(QString text READ getText WRITE setText NOTIFY textChanged)
    /** The LiveSvg instance.
     *
     * This property is meant to be used if you use an <a
     * href="https://doc.qt.io/archives/qt-6.0/qml-qtqml-models-instantiator.html">Instantiator</a>.
     */
    Q_PROPERTY(LiveSvg *svg READ getSvgItem WRITE setSvgItem NOTIFY svgChanged)

  public:
    explicit DynamicSvgAttribute(QObject *parent = nullptr);
    ~DynamicSvgAttribute();

    QString getNodeId() const;
    void setNodeId(QString id);
    NameValuePair *getAttribute();
    QString getText() const;
    void setText(QString value);

    // Register dynamically created DynamicSvgAttribute instances
    Q_INVOKABLE void setSvgItem(LiveSvg *);
    LiveSvg *getSvgItem() const;

  public slots:
    void refreshNode();

  signals:
    void nodeIdChanged();
    void attributeChanged();
    void valueChanged();
    void textChanged();
    void svgChanged();

  private:
    Q_DECLARE_PRIVATE(DynamicSvgAttribute)
    QScopedPointer<DynamicSvgAttributePrivate> const d_ptr;
};

class LiveSvgAttachedType : public QObject
{
    Q_OBJECT
    QML_ANONYMOUS
    Q_PROPERTY(QString overlayNodeId READ getOverlayNodeId WRITE
                       setOverlayNodeId NOTIFY overlayNodeIdChanged);

  public:
    explicit LiveSvgAttachedType(QObject *parent);
    QString getOverlayNodeId() const { return node_id_; }
    void setOverlayNodeId(QString node_id);

  signals:
    void overlayNodeIdChanged();

  private:
    QString node_id_;

    QMetaObject::Connection transform_changed_, node_id_changed_;

    void onParentChanged(QQuickItem *);
    void onTransformChanged(LiveSvgPrivate *);
};

/** Item for manipulating a SVG image at runtime.
 *
 * This item has two main features:
 *  1. Placing QML Items on rectangles defined in the SVG image
 *  2. Altering attributes of SVG XML elements
 *
 * The \c dynamic SVG layer is split off of the
 * SVG image and treated separately. The rest is rendered into a
 * background image. Each time one of the attributes in the
 * \c dynamic SVG layer is changed with the help of
 * DynamicSvgAttribute instances, this layer is rerendered.
 * The node id of the SVG elements can be edited in the
 * XML-Editor of inkscape.
 *
 * \code{.qml}
 *
 * LiveSvg {
 *    source: ":/mysvg.svg"
 *
 *    PdVariable {
 *        id: cos
 *        connection {
 *            path: "/osc/cos"
 *            period: 0.05
 *        }
 *    }
 *    DynamicSvgAttribute {
 *        nodeId: "oscillator_dynamic"
 *        attribute {
 *            name: "transform"
 *            value: cos.connected ? ("translate(" + cos.value + ")") : ""
 *        }
 *    }
 * }
 *
 * \endcode
 *
 * QML Items which have the \c LiveSvg.overlayNodeId attached
 * property set to an id of a \c rect element in the \c overlay layer
 * are resized and moved to cover the specified \c rect element.
 *
 * \code
 * LiveSvg {
 *    source: ":/mysvg.svg"
 *
 *    PdBar {
 *        LiveSvg.overlayNodeId: "barOverlay"
 *
 *        connection {
 *            path: "/osc/cos"
 *            period: 0.05
 *        }
 *    }
 * }
 * \endcode
 *
 * Note that QSvgRenderer only supports the tiny svg standard.
 * For instance, \c &lt;tspan&gt; elements are not supported.
 * In inkscape, you have to edit the text nodes and remove the
 * \c &lt;tspan&gt; wrappers around the text nodes:
 *
 * \code{.xml}
 * <!-- misaligned -->
 * <text id="text2">
 *  <tspan>
 *      Misaligned text
 *  </tspan>
 * </text>
 *
 * <!-- Okay with QSvgRenderer -->
 * <text id="text2">
 *      Properly aligned text
 * </text>
 * \endcode
 */
class LiveSvg : public QQuickPaintedItem
{
    Q_OBJECT
    QML_NAMED_ELEMENT(LiveSvg)
    QML_ATTACHED(Qt6QmlPdWidgets::LiveSvgAttachedType)
    Q_CLASSINFO("DefaultProperty", "dynamicAttributes")

    Q_PROPERTY(QRectF viewBox READ getViewBox NOTIFY viewBoxChanged)
    /// Source path of the svg. Can also be in a resource file.
    Q_PROPERTY(QString source READ getSource WRITE setSource NOTIFY
                       sourceChanged)  // Image
    /// Make black color white. Not recommended for production usage.
    Q_PROPERTY(
            bool invert READ getInvert WRITE setInvert NOTIFY invertedChanged)
    /** Default QML Property to collect DynamicSvgAttribute children.
     * Do not access directly.
     */
    Q_PROPERTY(QQmlListProperty<QObject> dynamicAttributes READ
                       getDynamicAttributeList)
  public:
    LiveSvg(QQuickItem *parent = 0);
    ~LiveSvg();

    void paint(QPainter *painter);
    Q_INVOKABLE QString getSource() const;

    bool getInvert() const;
    void setInvert(bool);
    void setSource(QString s);
    void clearSource();
    QQmlListProperty<QObject> getDynamicAttributeList();

    QRectF getViewBox() const;

    static LiveSvgAttachedType *qmlAttachedProperties(QObject *attachee);

  signals:
    void scaleChanged(QSizeF scale);
    void viewBoxChanged();
    void sourceChanged();
    void invertedChanged();

  private:
    Q_DECLARE_PRIVATE(LiveSvg);

    QScopedPointer<LiveSvgPrivate> const d_ptr;

    Q_DISABLE_COPY(LiveSvg)
};

/****************************************************************************/

}  // namespace Qt6QmlPdWidgets

#endif
