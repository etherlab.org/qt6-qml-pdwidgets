/*****************************************************************************
 *
 * Copyright (C) 2018-2022  Bjarne von Horn <vh@igh.de>,
 *                          Wilhelm Hagemeister<hm@igh.de>,
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the Qt6QmlPdWidgets library.
 *
 * The Qt6QmlPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The Qt6QmlPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Qt6QmlPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <Qt6QmlPdWidgets1/Qt6QmlPdWidgets1.h>

#include <git_revision_hash.h>

#include <QCoreApplication>
#include "QtPdCom_classes.h"
#include <QtPdCom1/Translator.h>
#include <QLocale>

#define STR(x) #x
#define QUOTE(x) STR(x)

const char *const Qt6QmlPdWidgets::qt6qmlpdwidgets_version_code =
        QUOTE(Qt6QmlPdWidgets_MAJOR) "." QUOTE(Qt6QmlPdWidgets_MINOR) "." QUOTE(Qt6QmlPdWidgets_RELEASE);

const char *const Qt6QmlPdWidgets::qt6qmlpdwidgets_full_version = GIT_REV;

bool Qt6QmlPdWidgets::loadTranslation(QTranslator& translator, const QLocale& locale)
{
    const char path[] = ":/de.igh.pd" QUOTE(Qt6QmlPdWidgets_MAJOR) "/i18n";
    const bool ans = translator.load(locale, "Qt6QmlPdWidgets", "_", path);
    // all widgets are in english per default, so no need to have a ts file
    if (locale.language() == QLocale::English)
        return true;
    return ans;
}

Qt6QmlPdWidgets::Translator::Translator(QQmlEngine *parent)
    : QObject(parent), parent_(parent)
{
    Qt6QmlPdWidgets::loadTranslation(qmlTranslator);
    QtPdCom::loadTranslation(qtpdcomTranslator);
    QCoreApplication::installTranslator(&qmlTranslator);
    QCoreApplication::installTranslator(&qtpdcomTranslator);
}

Qt6QmlPdWidgets::Translator *Qt6QmlPdWidgets::Translator::create(
    QQmlEngine *qmlEngine, QJSEngine * /*jsEngine*/)
{
    return new Translator(qmlEngine);
}

void Qt6QmlPdWidgets::Translator::setLanguage(QString lang)
{
    const auto locale = lang.isEmpty() ? QLocale() : QLocale(lang);
    Qt6QmlPdWidgets::loadTranslation(qmlTranslator, locale);
    QtPdCom::loadTranslation(qtpdcomTranslator, locale);
    parent_->retranslate();
}

void Qt6QmlPdWidgets::Translator::retranslate()
{
    parent_->retranslate();
}
