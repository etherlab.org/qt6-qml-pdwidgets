/*****************************************************************************
 *
 * Copyright (C) 2018-2025  Bjarne von Horn <vh@igh.de>,
 *                          Wilhelm Hagemeister<hm@igh.de>,
 *
 * This file is part of the Qt6QmlPdWidgets library.
 *
 * The Qt6QmlPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The Qt6QmlPdWidgets library is distributed in the hope that it will be
 *useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Qt6QmlPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_LIVESVG_P_H
#define PD_LIVESVG_P_H

#include <QDomDocument>
#include <QDomElement>
#include <QMap>
#include <QMouseEvent>
#include <QObject>
#include <QPixmap>
#include <QPointF>
#include <QQmlProperty>
#include <QSize>
#include <QSizeF>
#include <QString>
#include <QSvgRenderer>
#include <QTimer>
#include <QTransform>
#include <QVariantMap>
#include <QtQuick/QQuickPaintedItem>


namespace Qt6QmlPdWidgets {

class LiveSvg;
class DynamicSvgAttributePrivate
{
  public:
    DynamicSvgAttributePrivate() = default;
    QString nodeId_;
    QString text_;
    NameValuePair attribute_;

    void clear()
    {
        assigned_node_ = {};
        svg_ = nullptr;
    }
    void setNode(QDomElement node) { assigned_node_ = node; }
    void update();
    void updateText();
    LiveSvg *getSvgItem() const;

    LiveSvgPrivate *svg_ = nullptr;
    QDomElement assigned_node_;

    static void setSvgPointer(LiveSvg &svg, DynamicSvgAttribute &a);
};

class LiveSvgPrivate : public QObject
{
    Q_OBJECT
  public:
    LiveSvgPrivate(LiveSvg *);
    QDomElement findDynamicElement(const QString &id)
    {
        return findElementById(m_svgDynamicDoc.documentElement(), id);
    }

    void requestUpdate()
    {
        Q_Q(LiveSvg);
        q->update();
    }


    const QMap<QString, QVariantMap> &getOverlayElements() const
    {
        return overlayElements;
    }
    const QSvgRenderer &getOverlayRenderer() const { return renderer_; }
    const QTransform &getOverlayTransform() const { return overlay_transform_; }
    static LiveSvgPrivate *accessDPtr(LiveSvg &svg) { return svg.d_func(); }

  private:
    QDomDocument m_svgdoc;
    QDomDocument m_svgDynamicDoc;
    QSvgRenderer renderer_, dynamic_renderer_;
    QRectF viewBox;
    QPixmap backgroundPixmap; /**< Pixmap that stores the background. */
    QRectF rendererBounds;
    QTransform overlay_transform_;
    QMap<QString, QVariantMap> overlayElements;
    QString source;
    bool empty;  //* no pixmal
    bool invert;
    bool backgroundPixmapDirty;

    QDomElement findLayer(const QString &layerName, const QDomElement &parent);

    void findElementsWithAttribute(
            const QDomElement &elem,
            const QString &attr,
            QList<QDomElement> &foundElements);
    QDomElement
    findElementById(const QDomElement &elem, const QString &idValue);

    void getOverlayRects(const QDomElement &parent);
    void getTransformations(const QDomNode &elem, QPointF &offset);

    void printElements(QList<QDomElement> elements);
    void printAttributes(QDomElement elem);
    void updateBackgroundPixmap();

  signals:

    void transformChanged(LiveSvgPrivate *);

  private:
    LiveSvg *const q_ptr;
    Q_DECLARE_PUBLIC(LiveSvg);
    friend class DynamicSvgAttributePrivate;
    static void append(QQmlListProperty<QObject> *list, QObject *obj);
};

/****************************************************************************/

}  // namespace Qt6QmlPdWidgets

#endif
