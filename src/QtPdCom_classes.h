/*****************************************************************************
 *
 * Copyright (C) 2018-2024  Bjarne von Horn <vh@igh.de>,
 *                          Wilhelm Hagemeister<hm@igh.de>,
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the Qt6QmlPdWidgets library.
 *
 * The Qt6QmlPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The Qt6QmlPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Qt6QmlPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#pragma once

#include <QQmlEngine>
#include <QTranslator>

namespace Qt6QmlPdWidgets {

class Q_DECL_EXPORT Translator: public QObject {
    Q_OBJECT
    QML_NAMED_ELEMENT(PdTranslator)
    QML_SINGLETON

    QQmlEngine * const parent_;
    QTranslator qtpdcomTranslator, qmlTranslator;

        explicit Translator(QQmlEngine *parent);
    public:
        static Translator *create(QQmlEngine *qmlEngine, QJSEngine *jsEngine);

        Q_INVOKABLE void setLanguage(QString lang = {});
        Q_INVOKABLE void retranslate();
};

}
