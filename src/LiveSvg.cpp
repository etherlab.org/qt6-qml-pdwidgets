/*****************************************************************************
 *
 * Copyright (C) 2018-2025  Bjarne von Horn <vh@igh.de>,
 *                          Wilhelm Hagemeister<hm@igh.de>,
 *
 * This file is part of the Qt6QmlPdWidgets library.
 *
 * The Qt6QmlPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The Qt6QmlPdWidgets library is distributed in the hope that it will be
 *useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Qt6QmlPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/
/****************************************************************************
**
** Copyright (C) 2021-2023 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** !!!!!!!!!!!! this is work in progress !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
**
** livesvg bietet die Möglichkeit svgs zu manipulieren
**
** 1. der Layer mit Namen: "dynamic" kann Komponenten enthalten, bei denen
**    Attribute aus QML heraus gesetzt werden können. Diese Komponenten werden
**    über die svg:id gefunden.
**    Beispiel: im svg     <g id="traverse">....</g>
**
**              in qml
      LiveSvg {
            id: svg
            .....
            Slider {
                id: slider
            }

            DynamicSvgAttribute {
                nodeId: "traverse"
                attribute {
                    name: "transform"
                    value: "translate(0,"+slider.value+")"
                }
            }
      }
**
** 2. Im Layer "overlay" werde alle "rects" herausgesucht und alle
** "child" Componenten werden eingepasst, die im qml das Attached Property
"ZfLiveSvg.overlayNodeId" auf die id im svg-rect
** gesetzt haben.
**
**    Beispiel: im svg <rect
       id="pneumatic"
       width="1200"
       height="500"
       x="-3831.3479"
       y="1474.6614" />
                in qml

       LiveSvg {
            id:svg
            source: "qrc:/images/seitenansicht.svg"
            PdGauge {
          id:dbvGauge
          ZfLiveSvg.overlayNodeId:"pneumatic"
            }
       }

**
** TODO: die absolute Position eines "rects" ergibt sich durch die Kette aller
** Transformationen im SVG. Bisher ist nur die "translation" implementiert!
**
** Auch wichtig: qt.svg unterstützt nur das tiny svg format Version: 1.2.
** d.h. nicht alle svg-Elemente werden unterstützt
**
**
****************************************************************************/

#include "LiveSvg.h"

#include "LiveSvg_p.h"

#include <QDebug>
#include <QFile>
#include <QPainter>
#include <QQmlListReference>
#include <QQuickItem>
#include <QRectF>
#include <QSizeF>
#include <QtDebug>
#include <QtMath>

using Qt6QmlPdWidgets::DynamicSvgAttribute;
using Qt6QmlPdWidgets::DynamicSvgAttributePrivate;
using Qt6QmlPdWidgets::LiveSvg;
using Qt6QmlPdWidgets::LiveSvgAttachedType;
using Qt6QmlPdWidgets::LiveSvgPrivate;
using Qt6QmlPdWidgets::NameValuePair;


DynamicSvgAttribute::DynamicSvgAttribute(QObject *parent) :
    QObject(parent), d_ptr(new DynamicSvgAttributePrivate())
{
    Q_D(DynamicSvgAttribute);
    connect(&d->attribute_, &NameValuePair::valueChanged, this,
            [d]() { d->update(); });
    connect(&d->attribute_, &NameValuePair::nameChanged, this,
            [d]() { d->update(); });
}

DynamicSvgAttribute::~DynamicSvgAttribute() = default;

QString DynamicSvgAttribute::getNodeId() const
{
    const Q_D(DynamicSvgAttribute);
    return d->nodeId_;
}

void DynamicSvgAttribute::setNodeId(QString id)
{
    Q_D(DynamicSvgAttribute);
    if (id != d->nodeId_) {
        d->nodeId_ = id;
        refreshNode();
        emit nodeIdChanged();
    }
}

NameValuePair *DynamicSvgAttribute::getAttribute()
{
    Q_D(DynamicSvgAttribute);
    return &d->attribute_;
}

QString DynamicSvgAttribute::getText() const
{
    const Q_D(DynamicSvgAttribute);
    return d->text_;
}

void DynamicSvgAttribute::setText(QString text)
{
    Q_D(DynamicSvgAttribute);
    if (text != d->text_) {
        d->text_ = text;
        emit textChanged();
    }
    d->updateText();
}

void DynamicSvgAttribute::setSvgItem(LiveSvg *svg)
{
    DynamicSvgAttributePrivate::setSvgPointer(*svg, *this);
}

LiveSvg *DynamicSvgAttribute::getSvgItem() const
{
    const Q_D(DynamicSvgAttribute);
    return d->getSvgItem();
}

LiveSvg *DynamicSvgAttributePrivate::getSvgItem() const
{
    if (svg_)
        return svg_->q_func();
    return nullptr;
}

void DynamicSvgAttribute::refreshNode()
{
    Q_D(DynamicSvgAttribute);
    if (d->svg_)
        d->assigned_node_ = d->svg_->findDynamicElement(d->nodeId_);
    d->update();
}

void DynamicSvgAttributePrivate::update()
{
    if (attribute_.getName().isEmpty()) {
        if (!text_.isEmpty()) {
            updateText();
        }
        return;
    }
    assigned_node_.setAttribute(attribute_.getName(), attribute_.getValue());
    if (svg_)
        svg_->requestUpdate();
}

void DynamicSvgAttributePrivate::updateText()
{
    if (!assigned_node_.firstChild().isText())
        return;
    assigned_node_.firstChild().toText().setData(text_);
    if (svg_)
        svg_->requestUpdate();
}


/* ---------------------------------------------------------------------------------
 */

LiveSvg::LiveSvg(QQuickItem *parent) :
    QQuickPaintedItem(parent), d_ptr(new LiveSvgPrivate(this))
{
    //  this->setImplicitWidth(200);
    // this->setImplicitHeight(200);
}

/* ---------------------------------------------------------------------------------
 */

LiveSvg::~LiveSvg() = default;

/* ---------------------------------------------------------------------------------
 */

LiveSvgPrivate::LiveSvgPrivate(LiveSvg *This) :
    QObject(This),
    m_svgdoc("svg"),
    viewBox(0, 0, 0, 0),
    rendererBounds(0, 0, 0, 0),
    source(""),
    empty(true),
    invert(false),
    backgroundPixmapDirty(true),
    q_ptr(This)
{}

/* ---------------------------------------------------------------------------------
 */

void LiveSvgPrivate::append(QQmlListProperty<QObject> *list, QObject *obj)
{
    const auto svg = qobject_cast<LiveSvg *>(list->object);
    Q_ASSERT(svg);

    if (const auto attr = qobject_cast<DynamicSvgAttribute *>(obj)) {
        // Use DynamicSvgAttribute declaration for us.
        DynamicSvgAttributePrivate::setSvgPointer(*svg, *attr);
    }
    else {
        // forward to QQuickItem
        QQmlListReference ref(svg, "data");
        ref.append(obj);
    }
}

void DynamicSvgAttributePrivate::setSvgPointer(
        LiveSvg &svg,
        DynamicSvgAttribute &a)
{
    LiveSvgPrivate *svg_d = LiveSvgPrivate::accessDPtr(svg);
    if (a.d_func()->svg_ != svg_d) {
        a.d_func()->svg_ = svg_d;
        emit a.svgChanged();
    }
    QObject::connect(
            &svg, &LiveSvg::sourceChanged, &a,
            &DynamicSvgAttribute::refreshNode);
    QObject::connect(
            &svg, &QObject::destroyed, &a, [&a]() { a.d_func()->clear(); });
    a.refreshNode();
}

void LiveSvg::clearSource()
{
    Q_D(LiveSvg);
    d->overlayElements.clear();
    d->backgroundPixmapDirty = true;
}

QQmlListProperty<QObject> LiveSvg::getDynamicAttributeList()
{
    return QQmlListProperty<QObject>(
            this, nullptr, LiveSvgPrivate::append, nullptr, nullptr, nullptr);
}

void LiveSvg::setInvert(bool value)
{
    Q_D(LiveSvg);
    if (value != d->invert) {
        d->invert = value;
        d->backgroundPixmapDirty = true;
        emit invertedChanged();
        update(QRect(0, 0, this->width(), this->height()));
    }
}
/* ---------------------------------------------------------------------------------
 */

void LiveSvg::setSource(const QString s)
{
    Q_D(LiveSvg);
    // read svg-file (aka xml) in a DomDocument

    // to avoid a inconsistency between qml and c++ ressource file locators
    QString source(s);

    source = source.replace("qrc:", ":");

    if (d->source == source) {
        return;
    }

    d->source = source;

    d->empty = true;
    clearSource();

    QFile file(source);
    if (!file.open(QIODevice::ReadOnly)) {
        emit sourceChanged();
        qDebug() << "no file";
        return;
    }

    if (!d->m_svgdoc.setContent(&file)) {
        file.close();
        qDebug() << "svg could not be read";
        emit sourceChanged();
        return;
    }

    // set the dynamicDoc first to the same content
    d->m_svgDynamicDoc.setContent(d->m_svgdoc.toString());

    // and now delete the layer dynamic in the orginal doc
    {
        QDomElement elem =
                d->findLayer("dynamic", d->m_svgdoc.documentElement());
        if (!elem.isNull()) {
            elem.parentNode().removeChild(elem);
        }
    }

    // and only keep the layer dynamic in svgDynamicDoc
    // we can't just cut the dynamic layer out of the svgDoc because
    // then global definitions in the svgDoc get lost!
    {
        QList<QDomElement> groups;
        QDomElement elem;
        d->findElementsWithAttribute(
                d->m_svgDynamicDoc.documentElement(), "inkscape:groupmode",
                groups);
        foreach (elem, groups) {
            if (elem.attribute("inkscape:groupmode") == "layer") {
                if (elem.attribute("inkscape:label") != "dynamic") {
                    elem.parentNode().removeChild(elem);
                }
            }
        }
    }
    // get all recs in the Layer overlay
    d->getOverlayRects(d->m_svgdoc.documentElement());
    d->empty = false;
    d->backgroundPixmapDirty = true;
    emit sourceChanged();
    // d->findElementById(d->m_svgdoc.documentElement(),"bla");
}


/* ---------------------------------------------------------------------------------
 */

// Im SVG sollten sich mehrere Layer befinden. Nur der Layer, der mit msr
// markiert ist, wird dynamisch aktualisiert

// Im Layer Overlay befinden sich rects, die als Platzhalter für
// qml-Komponenten verwendet werden das Layer selber wird nicht gezeichnet

QDomElement
LiveSvgPrivate::findLayer(const QString &layerName, const QDomElement &parent)
{
    QList<QDomElement> groups;
    QDomElement elem;
    findElementsWithAttribute(parent, "inkscape:groupmode", groups);
    foreach (elem, groups) {
        if (elem.attribute("inkscape:groupmode") == "layer") {
            if (elem.attribute("inkscape:label") == layerName) {
                return elem;
            }
        }
    }
    return QDomElement();
}

/* ---------------------------------------------------------------------------------
 */

/* TODO we need to react to all transformations, right now only
   translations are taken into account!!
   Also the parser could be made with regular expressions....

   The parser is in:
   /vol/opt/qt/5.12.6/Src/qtsvg/src/svg/qsvghandler.cpp
   Function: static QMatrix parseTransformationMatrix(const QStringRef &value)

   !!!!
   Look at QMatrix QSvgRenderer::matrixForElement(const QString &id) const
   !!!! that should do as well and better ;-)
*/

void LiveSvgPrivate::getTransformations(const QDomNode &elem, QPointF &offset)
{
    if (!elem.parentNode().isNull()) {
        QString transform =
                elem.parentNode().toElement().attribute("transform");

        // FIXME where are all the other transformations: shear, scale,
        // matrix, rotate,....
        if (transform.contains("translate", Qt::CaseInsensitive)) {
            QStringList tList = transform.replace('"', "")
                                        .replace("translate", "")
                                        .replace('(', "")
                                        .replace(')', "")
                                        .split(',');  // FIXME regex?
            if (tList.size() == 2) {
                offset += QPointF(tList[0].toDouble(), tList[1].toDouble());
            }
        }

        getTransformations(elem.parentNode(), offset);
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvgPrivate::getOverlayRects(const QDomElement &parent)
{
    // find the layer which name is "overlay"
    QDomElement overlay = findLayer("overlay", parent);

    // get all the rects with id, x,y,width and height
    if (!overlay.isNull()) {
        // qDebug() <<  "overlay found";
        QDomNodeList rects = overlay.elementsByTagName("rect");
        for (int i = 0; i < rects.length(); i++) {
            QDomNamedNodeMap attr = rects.at(i).toElement().attributes();
            QVariantMap e;

            // get all translation offsets
            // FIXME take all transformations into account
            QPointF offset = QPointF(0, 0);
            getTransformations(rects.at(i), offset);
            // put into the map of the rect
            // we hope that in svg there will never be a "ox" and "oy"
            e["ox"] = offset.x();
            e["oy"] = offset.y();

            for (int j = 0; j < attr.count(); ++j) {
                QDomAttr attribute = attr.item(j).toAttr();
                e[attribute.name()] = attribute.value();
            }

            overlayElements[e["id"].toString()] = e;  // global in class
        }
        // qDebug() << "entries" <<  overlayElements;
        // qDebug() << "entries-------------------------";
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvgPrivate::updateBackgroundPixmap()
{
    Q_Q(LiveSvg);
    QPainter painter;

    backgroundPixmap = QPixmap(QSize(q->width(), q->height()));
    backgroundPixmap.fill(Qt::transparent);


    painter.begin(&backgroundPixmap);
    painter.setRenderHints(QPainter::Antialiasing);

    if (invert) {  // FIXME very dirty hack.....
        QString doc = m_svgdoc.toString();
        doc = doc.replace("#000000", "#ffffff");
        renderer_.load(doc.toUtf8());
    }
    else {
        renderer_.load(m_svgdoc.toByteArray());
    }
    viewBox = renderer_.viewBoxF();
    emit q->viewBoxChanged();

    // calc the bounds for preserveAspectFit (which is most likely wanted?!)
    // image is centered
    double scale =
            qMin(q->width() / viewBox.width(), q->height() / viewBox.height());

    rendererBounds =
            QRectF((q->width() - viewBox.width() * scale) / 2,
                   (q->height() - viewBox.height() * scale) / 2,
                   viewBox.width() * scale, viewBox.height() * scale);


    //        qDebug() << "svg viewbox" <<  viewBox;


    renderer_.render(&painter, rendererBounds);
    painter.end();
    if (viewBox.width() > 0 && viewBox.height() > 0) {
        overlay_transform_ = QTransform(
                scale, 0, 0, scale, rendererBounds.x() - viewBox.x() * scale,
                rendererBounds.y() - viewBox.y() * scale);
        emit transformChanged(this);
        // DBG getTransform(m_renderer);
        emit q->scaleChanged(QSizeF(scale, scale));
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::paint(QPainter *painter)
{
    Q_D(LiveSvg);
    // qDebug() << "paint size" << this->width() << " x " << this->height();
    if (!d->empty) {
        if (d->backgroundPixmap.size() != QSize(width(), height())
            || d->backgroundPixmapDirty) {
            d->updateBackgroundPixmap();
            d->backgroundPixmapDirty = false;
        }
    }
    painter->drawPixmap(0, 0, d->backgroundPixmap);

    // now the dynamic part
    d->dynamic_renderer_.load(d->m_svgDynamicDoc.toByteArray());
    d->dynamic_renderer_.render(painter, d->rendererBounds);
}

/* ---------------------------------------------------------------------------------
 */
/* thanks to
https://stackoverflow.com/questions/17836558/can-i-find-all-xml-nodes-with-a-given-attribute-in-a-qdomdocument
*/

void LiveSvgPrivate::findElementsWithAttribute(
        const QDomElement &elem,
        const QString &attr,
        QList<QDomElement> &foundElements)
{
    if (elem.attributes().contains(attr))
        foundElements.append(elem);

    QDomElement child = elem.firstChildElement();
    while (!child.isNull()) {
        findElementsWithAttribute(child, attr, foundElements);
        child = child.nextSiblingElement();
    }
}

/* ---------------------------------------------------------------------------------
 */

QDomElement
LiveSvgPrivate::findElementById(const QDomElement &elem, const QString &idValue)
{
    if (elem.attributes().contains("id")) {
        if (elem.attribute("id") == idValue) {
            return elem;
        }
    }

    QDomElement child = elem.firstChildElement();
    while (!child.isNull()) {
        QDomElement elem = findElementById(child, idValue);
        if (!elem.isNull())
            return elem;
        child = child.nextSiblingElement();
    }
    return QDomElement();
}

/* -----for debugging
 * ------------------------------------------------------------- */

void LiveSvgPrivate::printElements(QList<QDomElement> elements)
{
    QDomElement elem;
    qDebug() << "count: " << elements.count();
    foreach (elem, elements) {
        qDebug() << "Tagname" << elem.tagName();
        // and list all attributes
        printAttributes(elem);
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvgPrivate::printAttributes(QDomElement elem)
{
    QDomNamedNodeMap var = elem.attributes();
    for (int i = 0; i < var.count(); ++i) {
        QDomAttr attribute = var.item(i).toAttr();
        qDebug() << "Attr: " << attribute.name() << ": " << attribute.value();
    }
}

QString LiveSvg::getSource() const
{
    const Q_D(LiveSvg);
    return d->source;
}

bool LiveSvg::getInvert() const
{
    const Q_D(LiveSvg);
    return d->invert;
}

QRectF LiveSvg::getViewBox() const
{
    const Q_D(LiveSvg);
    return d->viewBox;
}

LiveSvgAttachedType *LiveSvg::qmlAttachedProperties(QObject *attachee)
{
    return new LiveSvgAttachedType(attachee);
}

LiveSvgAttachedType::LiveSvgAttachedType(QObject *parent) : QObject(parent)
{
    if (auto quick_item = qobject_cast<QQuickItem *>(parent)) {
        connect(quick_item, &QQuickItem::parentChanged, this,
                &LiveSvgAttachedType::onParentChanged);
    }
}

void LiveSvgAttachedType::setOverlayNodeId(QString const node_id)
{
    if (node_id == node_id_)
        return;

    node_id_ = node_id;

    if (!transform_changed_) {
        // parent LiveSvg has not been found, try again to find it
        onParentChanged(qobject_cast<QQuickItem *>(parent()));
    }

    emit overlayNodeIdChanged();
}

void LiveSvgAttachedType::onParentChanged(QQuickItem *quick_item)
{
    while ((quick_item = qobject_cast<QQuickItem *>(quick_item))) {
        if (const auto svg = qobject_cast<LiveSvg *>(quick_item)) {
            QObject::disconnect(transform_changed_);
            QObject::disconnect(node_id_changed_);
            const auto d = LiveSvgPrivate::accessDPtr(*svg);
            transform_changed_ = QObject::connect(
                    d, &LiveSvgPrivate::transformChanged, this,
                    &LiveSvgAttachedType::onTransformChanged);
            node_id_changed_ = QObject::connect(
                    this, &LiveSvgAttachedType::overlayNodeIdChanged, d,
                    [this, d]() { this->onTransformChanged(d); });
            return;
        }
        quick_item = quick_item->parentItem();
    }
}

void LiveSvgAttachedType::onTransformChanged(LiveSvgPrivate *svg)
{
    const auto item = qobject_cast<QQuickItem *>(parent());

    const auto pe = svg->getOverlayElements().find(node_id_);
    if (pe == svg->getOverlayElements().end() || !item) {
        qDebug() << "node" << node_id_ << "not found in svg.";
        return;
    }
    const auto e = *pe;

    if (!svg->getOverlayRenderer().elementExists(node_id_)) {
        if (svg->getOverlayRenderer().isValid())
            qDebug() << node_id_ << "does not exist";
        return;
    }

    const QRectF svg_rect(
            e["x"].toDouble(), e["y"].toDouble(), e["width"].toDouble(),
            e["height"].toDouble());

    const auto svg_transform =
            svg->getOverlayRenderer().transformForElement(e["id"].toString());

    if (svg_transform.isRotating()) {
        qWarning() << "Rotating Items is not supported";
    }
    const auto svg_transformed_rect = svg_transform.mapRect(svg_rect);
    const auto item_rect =
            svg->getOverlayTransform().mapRect(svg_transformed_rect);
    item->setX(item_rect.x());
    item->setY(item_rect.y());
    item->setHeight(item_rect.height());
    item->setWidth(item_rect.width());
}
